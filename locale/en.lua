-- Warhammer Online Wargames

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- wargames namespace
if not wargames then
  wargames = {}
  if not wargames.T then
    wargames.T = {}
  end
end

wargames.T[SystemData.Settings.Language.ENGLISH] = {
	tomecategoryname = L"Wargames",
	gems = {
		scorelabel = L"Score:",
		timelabel = L"Remaining Time:",
	},
	pairs = {
		ad_completed = L"Find all pairs.",
		ad_completed100 = L"Find all pairs one hundred times.",
		ad_row3 = L"Find three pairs without doing any mistakes.",
		ad_row5 = L"Find five pairs without doing any mistakes.",
		ad_turns100 = L"Complete a game in 100 or less turns.",
		ad_turns50 = L"Complete a game in 50 or less turns.",
		an_completed = L"A cakewalk ...",
		an_completed100 = L"Addicted",
		an_row3 = L"Hattrick!",
		an_row5 = L"High Five!",
		an_turns100 = L"Thinker",
		an_turns50 = L"Lucky Sod",
		tomepagedescription = L"Who doesn't know this simple game from his or her childhood days? Turn two cards and try to find pairs. If the cards don't match, they're turned back again. Try to finish the game in as few turns as possible.",
		tomepagename = L"Pairs",
	},
	ttt = {
		tomepagedescription = L"How about some quick round of Tic-Tac-Toe? Try to create a full row of 3 tiles without allowing your enemy to do so.",
		tomepagename = L"Tic-Tac-Toe",
	},
}
