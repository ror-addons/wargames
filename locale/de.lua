-- Warhammer Online Wargames

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- wargames namespace
if not wargames then
  wargames = {}
  if not wargames.T then
    wargames.T = {}
  end
end

wargames.T[SystemData.Settings.Language.GERMAN] = {
	tomecategoryname = L"Kriegsspiele",
	gems = {
		scorelabel = L"Punktestand:",
		timelabel = L"Verbleibende Zeit:",
	},
	pairs = {
		ad_completed = L"Deckt alle Paare auf.",
		ad_completed100 = L"Spielt 100 komplette Runden.",
		ad_row3 = L"Deckt drei Kartenpaare in Folge auf, ohne Fehler zu machen.",
		ad_row5 = L"Deckt f�nf Kartenpaare in Folge auf, ohne Fehler zu machen.",
		ad_turns100 = L"Deckt alle Paare in h�chstens 100 Z�gen auf.",
		ad_turns50 = L"Deckt alle Paare in h�chstens 50 Z�gen auf.",
		an_completed = L"Ein Kinderspiel ...",
		an_completed100 = L"S�chtling",
		an_row3 = L"Hattrick!",
		an_row5 = L"Gib mir F�nf!",
		an_turns100 = L"Denker",
		an_turns50 = L"Gl�ckspilz",
		tomepagedescription = L"Wer kennt dieses Spiel nicht aus Kindertagen? Es werden immer zwei Karten aufgedeckt. Sind beide identisch, bleiben sie so liegen. Sind sie nicht identisch, werden sie wieder verdeckt. Das Spiel endet, wenn alle Karten bzw. alle Paare aufgedeckt wurden.",
		tomepagename = L"Pairs",
	},
	ttt = {
		tomepagedescription = L"Wie w�re es mit einer schnellen Runde Tic-Tac-Toe? Bildet eine vollst�ndige Reihe aus 3 Feldern, aber hindert gleichzeitig Euren Gegner daran, dies zu tun.",
		tomepagename = L"Tic-Tac-Toe",
	},
}
