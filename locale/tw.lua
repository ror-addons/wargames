-- Warhammer Online Wargames

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- Traditional Chinese translation done by mymag

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- wargames namespace
if not wargames then
  wargames = {}
  if not wargames.T then
    wargames.T = {}
  end
end

wargames.T[SystemData.Settings.Language.T_CHINESE] = {
	tomecategoryname = L"戰鎚小遊戲",
	gems = {
		scorelabel = L"分數:",
		timelabel = L"剩餘時間:",
	},
	pairs = {
		ad_completed = L"找到所有牌子",
		ad_completed100 = L"玩了一百次翻牌遊戲", -- Needs review
		ad_row3 = L"連續找到三次牌子",
		ad_row5 = L"連續找到五次牌子",
		ad_turns100 = L"完成遊戲次數100次",
		ad_turns50 = L"完成遊戲50次",
		an_completed = L"一個比賽...",
		an_completed100 = L"上癮了",
		an_row3 = L"帽子戲法", -- Needs review
		an_row5 = L"最高五次!",
		an_turns100 = L"思想家",
		an_turns50 = L"幸運草",
		tomepagedescription = L"誰小時候沒玩過這個遊戲？翻起兩張牌子然後找到一樣的牌子，如果兩張牌子不一樣就會翻回去，重新再找，輪流嘗試好完成這個比賽", -- Needs review
		tomepagename = L"翻牌遊戲",
	},
	ttt = {
		tomepagedescription = L"看誰最快完成井字遊戲？輪流放置一格，達到三個為一連線就贏了。", -- Needs review
		tomepagename = L"井字遊戲",
	},
}
