-- Warhammer Online Wargames

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- wargames namespace
if not wargames then
  wargames = {}
  if not wargames.T then
    wargames.T = {}
  end
end

wargames.T[SystemData.Settings.Language.SPANISH] = {
	tomecategoryname = L"Wargames",
	gems = {
		scorelabel = L"Puntuaci�n",
		timelabel = L"Tiempo restante",
	},
	pairs = {
		ad_completed = L"Encuentra todas las parejas",
		ad_completed100 = L"Encuentra todas las parejas 100 veces",
		ad_row3 = L"Encuentra 3 parejas sin cometer ning�n error",
		ad_row5 = L"Choca esos 5!",
		ad_turns100 = L"Pensador",
		ad_turns50 = L"Imprudente con suerte",
		an_completed = L"Pan comido",
		an_completed100 = L"Adicto",
		an_row3 = L"Hattrick!",
		an_row5 = L"Choca esos 5!",
		an_turns100 = L"Pensador",
		an_turns50 = L"Imprudente con suerte",
		tomepagedescription = L"�Qui�n no conoce este juego desde que era ni�o? Da la vuelta a 2 cartas y trata de que sean las mismas. Si las cartas no coinciden, volver�n a ponerse boca abajo. Trata de terminar el juego en el menor n�mero de turnos posible.",
		tomepagename = L"Parejas",
	},
	ttt = {
		tomepagedescription = L"�Te apetece una partida r�pida al tres en raya? Trata de conseguir una columna de 3 casillas amarillas evitando que tu enemigo haga lo mismo con las azules.",
		tomepagename = L"Tres en raya",
	},
}
