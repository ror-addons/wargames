-- Warhammer Online Wargames

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- French translation done by sdrtrax

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- wargames namespace
if not wargames then
  wargames = {}
  if not wargames.T then
    wargames.T = {}
  end
end

wargames.T[SystemData.Settings.Language.FRENCH] = {
	tomecategoryname = L"Jeux de guerre",
	gems = {
		scorelabel = L"Score:", -- Requires localization
		timelabel = L"Remaining Time:", -- Requires localization
	},
	pairs = {
		ad_completed = L"Trouvez toutes les paires",
		ad_completed100 = L"Trouvez toutes les paires une centaine de fois.",
		ad_row3 = L"Trouvez trois paires sans faire d'erreurs.",
		ad_row5 = L"Trouvez cinq paires sans faire d'erreurs.",
		ad_turns100 = L"Finir un jeu en 100 tours ou moins",
		ad_turns50 = L"Finir un jeu en 50 tous ou moins",
		an_completed = L"A cakewalk ...",
		an_completed100 = L"Accro",
		an_row3 = L"Hattrick!",
		an_row5 = L"Tape en cinq!",
		an_turns100 = L"Le penseur",
		an_turns50 = L"Veinard",
		tomepagedescription = L"Qui n'a jamais jou� � ce jeu durant son enfance? Tournez deux cartes et trouvez une paire. Si les cartes ne correspondent pas, elles seront tourn�es � nouveau. Essayez de finir le jeu en moins de tours possible.",
		tomepagename = L"Paires",
	},
	ttt = {
		tomepagedescription = L"Que dire sur une petite partie de Tic-Tac-Toe? Essayez de player une ligne compl�te avant que votre ennemi ne le fasse.",
		tomepagename = L"Tic-Tac-Toe",
	},
}
