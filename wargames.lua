-- Warhammer Online Wargames

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/wargames.T

-- wargames namespace
if not wargames then
  wargames = {}
end
if not wargames.storage then
  wargames.storage = {}
end
if not wargames.games then
  wargames.games = {}
end
if not wargames.T then
  wargames.T = {}
end

--============================================================================--

function wargames.OnInitialize()
  -- init localization
  if wargames.T[SystemData.Settings.Language.active] ~= nil then
    wargames.T = wargames.T[SystemData.Settings.Language.active]
  else
    wargames.T = wargames.T[SystemData.Settings.Language.ENGLISH]
  end

  if LibSlash then
    LibSlash.RegisterSlashCmd("play", wargames.PlayGame)
    LibSlash.RegisterSlashCmd("wg", wargames.PlayGame)
    LibSlash.RegisterSlashCmd("wargames", wargames.PlayGame)
  end
  
  for i,v in pairs(wargames.games) do
    if DoesWindowExist(v.window) then
      DestroyWindow(v.window)
      --LayoutEditor.UnregisterWindow(v.window)
    end
    CreateWindow(v.window, false)
    --LayoutEditor.RegisterWindow(v.window, v.name, v.description, false, false, true, nil)
    if v.init then v.init() end
  end
end

function wargames.OnShutdown()
  for i,v in pairs(wargames.games) do
    if DoesWindowExist(v.window) then
      DestroyWindow(v.window)
      --LayoutEditor.UnregisterWindow(v.window)
    end
	if v.quit then v.quit() end
  end
end

function wargames.OnUpdate()
  for i,v in pairs(wargames.games) do
	if v.update and WindowGetShowing(v.window) then v.update() end
  end
end

function wargames.RegisterGame(name, description, window, command, init, quit, update)
  wargames.games[command] = {}
  wargames.games[command].name = name
  wargames.games[command].description = description
  wargames.games[command].window = window
  wargames.games[command].init = init
  wargames.games[command].quit = quit
  wargames.games[command].update = update
  
  d(L"Game '" .. name .. L"' registered")
end

function wargames.PlayGame(command)
  if type(command) == "string" then
    command = towstring(command)
  end
  
  if wargames.games[command] then
    WindowSetShowing(wargames.games[command].window, true)
  else
    d(L"Unknown game/command: "..command)
  end
end
