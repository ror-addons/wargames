<?xml version="1.0" encoding="UTF-8"?>
<!-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
     http://creativecommons.org/licenses/by-nc-sa/3.0/ -->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Wargames" version="0.6" date="19/08/2009">
        <Author name="Tempest" email="icona@gmx.net" />
        <Description text="Bored? Waiting for enemies? Play some games!" />
		<VersionSettings gameVersion="1.3.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibAchievements" optional="true" />
			<Dependency name="LibSlash" optional="true" />
		</Dependencies>
        <Files>
			<File name="wargames.lua" />
			<File name="locale/en.lua" />
			<File name="locale/de.lua" />
			<File name="locale/fr.lua" />
			<File name="locale/tw.lua" />
			<File name="locale/es.lua" />
			<File name="games/pairs.xml" />
			<File name="games/ttt.xml" />
			<File name="games/gems.xml" />
		</Files>
        <OnInitialize>
            <CallFunction name="wargames.OnInitialize" />
        </OnInitialize>
		<OnUpdate>
		    <CallFunction name="wargames.OnUpdate" />
		</OnUpdate>
		<OnShutdown>
		    <CallFunction name="wargames.OnShutdown" />
		</OnShutdown>
		<SavedVariables>
			<SavedVariable name="wargames.storage" global="true" />
		</SavedVariables>
		<WARInfo>
			<Categories>
				<Category name="RVR" />
				<Category name="GROUPING" />
				<Category name="OTHER" />
			</Categories>
		</WARInfo>
    </UiMod>
</ModuleFile> 