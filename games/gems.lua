-- Warhammer Online Wargames Gems

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- wargames namespace
wargames.gems = {}
wargames.gems.flip = -1

local MAX_TIME = 30000

local animcounter = 0

local function InitStorage()
  if not wargames.storage.gems then
    wargames.storage.gems = {}
    wargames.storage.gems.firsttime = true
    wargames.storage.gems.firsticonindex = 393
    wargames.storage.gems.fields = {}
    wargames.storage.gems.stats_played = 0
    wargames.storage.gems.stats_length = 0
    wargames.storage.gems.stats_count = 0
    wargames.storage.gems.stats_score = 0
    wargames.storage.gems.count = 0
    wargames.storage.gems.score = 0
    wargames.storage.gems.numstones = 5
	wargames.storage.gems.time = MAX_TIME / 2
  end
end

local function GetFieldWindowForFieldNumber( fieldNumber )  
  local row = math.ceil(fieldNumber / 8)
  local col = fieldNumber - ((row - 1) * 8)
  return "WargamesGemsFieldsRow"..row.."Col"..col
end

local function GetFieldNumberForFieldWindow( fieldWindowName )  
  local col = WindowGetId( fieldWindowName )
  local row = WindowGetId( WindowGetParent( fieldWindowName ) )
  return col + (row - 1) * 8
end

local function GetFieldNumberForCurrentWindow()  
  local fieldWindowName = SystemData.ActiveWindow.name
  return GetFieldNumberForFieldWindow( fieldWindowName ) 
end

local function DrawField(i)
  if wargames.storage.gems.fields[i] ~= 0 then
    local texture, x, y = GetIconData(wargames.storage.gems.fields[i])
    DynamicImageSetTexture(GetFieldWindowForFieldNumber(i).."Icon", texture, x, y)
  else
    DynamicImageSetTexture(GetFieldWindowForFieldNumber(i).."Icon", "", 0, 0)
  end
end

local function PrepareGame()
  for i = 1, 64 do
    DrawField(i)
    local bn = GetFieldWindowForFieldNumber(i)
    WindowSetShowing(bn.."Text", false)
    WindowSetGameActionData(bn, 0, 0, L"")
    WindowSetShowing(bn.."Cooldown", false)
    WindowSetShowing(bn.."CooldownTimer", false)
  end
  d("Gems prepared")
end

local function ResetGame()
  wargames.gems.flip = -1
  wargames.storage.gems.fields = {}
  wargames.storage.gems.count = 0
  wargames.storage.gems.score = 0
  wargames.storage.gems.time = MAX_TIME / 2
  
  -- todo: reset
  for i = 1, 64 do
    wargames.storage.gems.fields[i] = wargames.storage.gems.firsticonindex + math.random(wargames.storage.gems.numstones) - 1
	WindowSetTintColor(GetFieldWindowForFieldNumber(i).."Icon",255,255,255)
  end
  
  d("Gems reset")
end

local function SwapFields(first, second)
  local temp = wargames.storage.gems.fields[first]
  wargames.storage.gems.fields[first] = wargames.storage.gems.fields[second]
  wargames.storage.gems.fields[second] = temp
  DrawField(first)
  DrawField(second)
end

local function ClearFields()
  local clear = {}
  
  local last = 0
  local len = 0
  local s
  
  local cleared_fields = 0
  local cleared_rows = 0
  
  --find horizontal rows
  for y = 1,8 do
    last = 0
	len = 0
    for x = 1,8 do
	  s = (y - 1) * 8 + x
      if wargames.storage.gems.fields[s] ~= last then
	    if len > 2 then
		  cleared_rows = cleared_rows + 1
		  for i = s - len,s - 1 do
		    clear[i] = true
		  end
		end
		len = 0
	  end
      last = wargames.storage.gems.fields[s]
	  len = len + 1
    end
    if len > 2 then
	  cleared_rows = cleared_rows + 1
	  for i = s - len + 1,s do
	    clear[i] = true
	  end
	end
  end

  --find vertical rows
  for x = 1,8 do
    last = 0
	len = 0
    for y = 1,8 do
	  s = (y - 1) * 8 + x
      if wargames.storage.gems.fields[s] ~= last then
	    if len > 2 then
		  cleared_rows = cleared_rows + 1
		  for i = s - len * 8,s - 8,8 do
		    clear[i] = true
		  end
		end
		len = 0
	  end
      last = wargames.storage.gems.fields[s]
	  len = len + 1
    end
    if len > 2 then
	  cleared_rows = cleared_rows + 1
	  for i = s - len * 8 + 8,s,8 do
		clear[i] = true
	  end
	end
  end

  local cleared = false
  
  for y = 1,8 do
    last = 0
	len = 0
    for x = 1,8 do
	  s = (y - 1) * 8 + x
	  --wargames.storage.gems.fields[s] =
	  if clear[s] and wargames.storage.gems.fields[s] ~= 0 then
		wargames.storage.gems.fields[s] = 0
		cleared_fields = cleared_fields + 1
		DrawField(s)
        cleared = true
	  end
    end
  end
  
  return cleared_rows, cleared_fields
end

local function IsFalling()
  for i = 1, 64 do
    if wargames.storage.gems.fields[i] == 0 then
	  return true
	end
  end
  return false
end

local function DoFalling()
  for x = 1,8 do
    for y = 8,2,-1 do
	  s = (y - 1) * 8 + x
	  if wargames.storage.gems.fields[s] == 0 and wargames.storage.gems.fields[s - 8] ~= 0 then
	    SwapFields(s, s - 8)
	  end
	end
  end
  
  for i = 1,8 do
    if wargames.storage.gems.fields[i] == 0 then
	  wargames.storage.gems.fields[i] = wargames.storage.gems.firsticonindex + math.random(wargames.storage.gems.numstones) - 1
	  DrawField(i)
	end
  end
end

local oldBarValue = 0

--local 
function TimeBar(value)
  if value == oldBarValue then return end
  
  if value <= 25 then
    StatusBarSetForegroundTint("WargamesGemsTimeStatusBar", 164, 81, 0)
  else
    if value <= 75 then
	  StatusBarSetForegroundTint("WargamesGemsTimeStatusBar", 255, 210, 0)
	else
	  StatusBarSetForegroundTint("WargamesGemsTimeStatusBar", 81, 164, 0)
    end
  end
  oldBarValue = value

  StatusBarSetCurrentValue("WargamesGemsTimeStatusBar", value)
end

function wargames.gems.OnInitialize()
  InitStorage()
  LabelSetText("WargamesGemsTitleBarText", L"Wargames Gems")
  LabelSetText("WargamesGemsScoreText", wargames.T.gems.scorelabel)
  LabelSetText("WargamesGemsTimeText", wargames.T.gems.timelabel)
  
  StatusBarSetMaximumValue("WargamesGemsTimeStatusBar", 100)
  StatusBarSetBackgroundTint("WargamesGemsTimeStatusBar", 20, 20, 20)
  
  if wargames.storage.gems.firsttime then
    wargames.storage.gems.firsttime = false
	ResetGame()
  end
  PrepareGame()

  if LibAchievements then
  if false then
    local wgcat = LibAchievements.CreateCategory(wargames.T.tomecategoryname)
    local gems_page = LibAchievements.CreatePage(wgcat,wargames.T.gems.tomepagename, true, wargames.T.gems.tomepagedescription)
  end
  end
end

function wargames.gems.OnShutdown()
  
end

function wargames.gems.Hide()
  WindowSetShowing("WargamesGems", false);
end

function wargames.gems.FieldLButtonDown()
  if IsFalling() then
	return
  end

  if wargames.storage.gems.time == 0 then
    ResetGame()
	PrepareGame()
	return
  end
  
  if wargames.gems.flip == -1 then
    wargames.gems.flip = GetFieldNumberForCurrentWindow()
	WindowSetTintColor(GetFieldWindowForFieldNumber(wargames.gems.flip).."Icon",64,64,64)
	PlaySound(120)
  else
    WindowSetTintColor(GetFieldWindowForFieldNumber(wargames.gems.flip).."Icon",255,255,255)
    local second = GetFieldNumberForCurrentWindow()
	PlaySound(120)
    if wargames.gems.flip ~= second then
      if math.abs(wargames.gems.flip - second) < 2 or math.abs(wargames.gems.flip - second) == 8 then
	    SwapFields(wargames.gems.flip, second)
		local r,f = ClearFields()
		if r == 0 then
		  wargames.storage.gems.score = math.max(0, wargames.storage.gems.score - 100)
		else
		  PlaySound(200)
		  
		  wargames.storage.gems.score = wargames.storage.gems.score + f * r * 10
		  
		  wargames.storage.gems.time = math.min(wargames.storage.gems.time + f * r * 100, MAX_TIME)
		  
		  wargames.storage.gems.count = wargames.storage.gems.count + f
		end
	  end
	end
	wargames.gems.flip = -1
  end
end

function wargames.gems.FieldLButtonUp()

end

function wargames.gems.FieldRButtonDown()

end

function wargames.gems.FieldMouseOver()

end

function wargames.gems.OnUpdate()
  animcounter = animcounter + 1
  
  if not IsFalling() then
    wargames.storage.gems.time = math.max(0, wargames.storage.gems.time - 1 - wargames.storage.gems.count / 100)
  end
  
  TimeBar(math.floor(wargames.storage.gems.time * 100 / MAX_TIME))
  
  if animcounter < 10 then
    return
  end
  animcounter = 0

  if IsFalling() then
    DoFalling()
	return
  end

  local r,f = ClearFields()
  if f > 0 then
    PlaySound(200)
    wargames.storage.gems.score = wargames.storage.gems.score + f * r * 15
    wargames.storage.gems.time = math.min(wargames.storage.gems.time + f * r * 100, MAX_TIME)
    wargames.storage.gems.count = wargames.storage.gems.count + f
  end
  
  if wargames.storage.gems.time == 0 then
    for i = 1,64 do
	  WindowSetTintColor(GetFieldWindowForFieldNumber(i).."Icon",164,81,0)
	end
  end
  
  LabelSetText("WargamesGemsScoreDisplayText",L""..wargames.storage.gems.score)
end

wargames.RegisterGame(L"Wargames Gems", L"...", "WargamesGems", L"gems", wargames.gems.OnInitialize, wargames.gems.OnShutdown, wargames.gems.OnUpdate)
