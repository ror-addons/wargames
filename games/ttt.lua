-- Warhammer Online Wargames Tic-Tac-Toe

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/wargames.T

-- wargames namespace
wargames.ttt = {}

local function InitStorage()
  if not wargames.storage.ttt then
    wargames.storage.ttt = {}
    wargames.storage.ttt.fields = {0, 0, 0, 0, 0, 0, 0, 0, 0}
    wargames.storage.ttt.firsttime = true
    wargames.storage.ttt.played = 0
    wargames.storage.ttt.stats_played = 0
    wargames.storage.ttt.stats_won = 0
    wargames.storage.ttt.stats_lost = 0
  end
end

local NONE = 0
local PLAYER = -1
local COMPUTER = 1
local PLAYERICON = 56
local COMPUTERICON = 55

local MAXDEPTH = 2

local function GetFieldWindowForFieldNumber( fieldNumber )  
  local row = math.ceil(fieldNumber / 3)
  local col = fieldNumber - ((row - 1) * 3)
  return "WargamesTTTFieldsRow"..row.."Col"..col
end

local function GetFieldNumberForFieldWindow( fieldWindowName )  
  local col = WindowGetId( fieldWindowName )
  local row = WindowGetId( WindowGetParent( fieldWindowName ) )
  return col + (row - 1) * 3
end

local function GetFieldNumberForCurrentWindow()  
  local fieldWindowName = SystemData.ActiveWindow.name
  return GetFieldNumberForFieldWindow( fieldWindowName ) 
end

local function PrepareGame()
  for i = 1, 9 do
    local bn = GetFieldWindowForFieldNumber(i)
    if wargames.storage.ttt.fields[i] == PLAYER then
      local texture, x, y = GetIconData(PLAYERICON)
      DynamicImageSetTexture(GetFieldWindowForFieldNumber(i).."Icon", texture, x, y)
    else
      if wargames.storage.ttt.fields[i] == COMPUTER then
        local texture, x, y = GetIconData(COMPUTERICON)
        DynamicImageSetTexture(GetFieldWindowForFieldNumber(i).."Icon", texture, x, y)
    else
      DynamicImageSetTexture (GetFieldWindowForFieldNumber(i).."Icon", "", 0, 0)
    end
  end

  WindowSetShowing(bn.."Text", false)
  WindowSetGameActionData(bn, 0, 0, L"")
  WindowSetShowing(bn.."Cooldown", false)
  WindowSetShowing(bn.."CooldownTimer", false)
  end
  d("TTT prepared")
end

local function ResetGame()
  wargames.storage.ttt.fields = {0, 0, 0, 0, 0, 0, 0, 0, 0}
  wargames.storage.ttt.played = 0
  if math.random(2) == 2 then
    wargames.storage.ttt.played = 1
    wargames.storage.ttt.fields[math.random(9)] = COMPUTER
  end
  d("TTT reset")
end

local function FlipField(fieldNum)
--[[  PlaySound(120)
  wargames.storage.pairs.flipped[fieldNum] = not wargames.storage.pairs.flipped[fieldNum]
  if wargames.storage.pairs.flipped[fieldNum] then
    local texture, x, y = GetIconData(wargames.storage.pairs.fields[fieldNum])
    DynamicImageSetTexture(GetFieldWindowForFieldNumber(fieldNum + 1).."Icon", texture, x, y)
  else
    DynamicImageSetTexture (GetFieldWindowForFieldNumber(fieldNum + 1).."Icon", "", 0, 0)
  end]]--
end

local function GetWinningLine()
  local combinations = {
    {1, 2, 3},
    {4, 5, 6},
    {7, 8, 9},
    {1, 4, 7},
    {2, 5, 8},
    {3, 6, 9},
    {1, 5, 9},
    {3, 5, 7}
  }
  for i,v in pairs(combinations) do
    if wargames.storage.ttt.fields[v[1]] ~= 0 and wargames.storage.ttt.fields[v[1]] == wargames.storage.ttt.fields[v[2]] and wargames.storage.ttt.fields[v[2]] == wargames.storage.ttt.fields[v[3]] then return v end
  end
  return nil
end

function wargames.ttt.SimMax(depth, top)
  local value = -1000
  for i = 1, 9 do
    if wargames.storage.ttt.fields[i] == 0 then
      wargames.storage.ttt.fields[i] = COMPUTER
	  wargames.storage.ttt.played = wargames.storage.ttt.played + 1
	  local turn = 0
	  local temp = GetWinningLine()
	  if depth <= 1 or wargames.storage.ttt.played == 9 or temp ~= nil then
		if temp == nil then
		  turn = 0
		else
  		  turn = temp[1]
		end
	  else
	    turn = wargames.ttt.SimMin(depth - 1)
      end
	  wargames.storage.ttt.played = wargames.storage.ttt.played - 1
	  wargames.storage.ttt.fields[i] = 0
	  if turn > value then
	    value = turn
		if top then wargames.ttt.next = i end
	  end
    end
  end
  return value
end

function wargames.ttt.SimMin(depth)
  local value = 1000
  for i = 1, 9 do
    if wargames.storage.ttt.fields[i] == 0 then
      wargames.storage.ttt.fields[i] = PLAYER
	  wargames.storage.ttt.played = wargames.storage.ttt.played + 1
	  local turn = 0
	  local temp = GetWinningLine()
	  if depth <= 1 or wargames.storage.ttt.played == 9 or temp ~= nil then
		if temp == nil then
		  turn = 0
		else
		  turn = temp[1] * -1
		end
	  else
	    turn = wargames.ttt.SimMax(depth - 1, false)
	  end
	  wargames.storage.ttt.played = wargames.storage.ttt.played - 1
	  wargames.storage.ttt.fields[i] = 0
	  if turn < value then
	    value = turn
	  end
    end
  end
  return value
end

local function ComputerRound()
  wargames.ttt.next = -1
  wargames.ttt.SimMax(2,true)

  if wargames.ttt.next ~= -1 then
    wargames.storage.ttt.fields[wargames.ttt.next] = COMPUTER
    local texture, x, y = GetIconData(COMPUTERICON)
    DynamicImageSetTexture(GetFieldWindowForFieldNumber(wargames.ttt.next).."Icon", texture, x, y)
    wargames.storage.ttt.played = wargames.storage.ttt.played + 1
  end
end

local function DrawWinner()
  local win = GetWinningLine()
  if win ~= nil then
    wargames.storage.ttt.played = 9
    WindowSetShowing(GetFieldWindowForFieldNumber(win[1]).."Cooldown", true)
    WindowSetShowing(GetFieldWindowForFieldNumber(win[2]).."Cooldown", true)
    WindowSetShowing(GetFieldWindowForFieldNumber(win[3]).."Cooldown", true)

    wargames.storage.ttt.stats_played = wargames.storage.ttt.stats_played + 1
	
	if wargames.storage.ttt.fields[win[1]] == PLAYER then
	  WindowSetTintColor(GetFieldWindowForFieldNumber(win[1]).."Cooldown", 255, 255, 128)
	  WindowSetTintColor(GetFieldWindowForFieldNumber(win[2]).."Cooldown", 255, 255, 128)
	  WindowSetTintColor(GetFieldWindowForFieldNumber(win[3]).."Cooldown", 255, 255, 128)
	  wargames.storage.ttt.stats_won = wargames.storage.ttt.stats_won + 1
	else
	  WindowSetTintColor(GetFieldWindowForFieldNumber(win[1]).."Cooldown", 128, 128, 255)
	  WindowSetTintColor(GetFieldWindowForFieldNumber(win[2]).."Cooldown", 128, 128, 255)
	  WindowSetTintColor(GetFieldWindowForFieldNumber(win[3]).."Cooldown", 128, 128, 255)
	  wargames.storage.ttt.stats_lost = wargames.storage.ttt.stats_lost + 1
	end
  end
end

function wargames.ttt.OnInitialize()
  InitStorage()
-- Window is too small right now
--  LabelSetText("WargamesTTTTitleBarText", L"Wargames Tic-Tac-Toe")
  if wargames.storage.ttt.firsttime then
    wargames.storage.ttt.firsttime = false
    ResetGame()
  end
  PrepareGame()
  DrawWinner()

  if LibAchievements then
--    local wgcat = LibAchievements.CreateCategory(wargames.T.tomecategoryname)
--    local ttt_page = LibAchievements.CreatePage(wgcat,wargames.T.ttt.tomepagename, true, wargames.T.ttt.tomepagedescription)
  end
end

function wargames.ttt.OnShutdown()
  
end

function wargames.ttt.Hide()
  WindowSetShowing("WargamesTTT", false);
end

function wargames.ttt.FieldLButtonDown()
  local i = GetFieldNumberForCurrentWindow()
  if wargames.storage.ttt.played == 9 then
    ResetGame()
    PrepareGame()
  else
    if wargames.storage.ttt.fields[i] == 0 then
      wargames.storage.ttt.fields[i] = PLAYER
      local texture, x, y = GetIconData(PLAYERICON)
      DynamicImageSetTexture(GetFieldWindowForFieldNumber(i).."Icon", texture, x, y)
      wargames.storage.ttt.played = wargames.storage.ttt.played + 1
      if wargames.storage.ttt.played ~= 9 and GetWinningLine() == nil then
        ComputerRound()
      end
    end
  end
  DrawWinner()
end

function wargames.ttt.FieldLButtonUp()
  
end

function wargames.ttt.FieldRButtonDown()
  
end

function wargames.ttt.FieldMouseOver()
  
end

wargames.RegisterGame(L"Wargames Tic-Tac-Toe", L"How about playing some Tic-Tac-Toe?", "WargamesTTT", L"ttt", wargames.ttt.OnInitialize, wargames.ttt.OnShutdown, nil)
