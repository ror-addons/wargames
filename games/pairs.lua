-- Warhammer Online Wargames Pairs

-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- wargames namespace
wargames.pairs = {}
wargames.pairs.flip0 = -1
wargames.pairs.flip1 = -1

local function InitStorage()
  if not wargames.storage.pairs then
    wargames.storage.pairs = {}
    wargames.storage.pairs.firsticonindex = 200
    wargames.storage.pairs.cards = {}
    wargames.storage.pairs.flipped = {}
    wargames.storage.pairs.solved = 0
    wargames.storage.pairs.firsttime = true
    wargames.storage.pairs.stats_completed = 0
    wargames.storage.pairs.stats_row = 0
    wargames.storage.pairs.stats_turns = 9999999
    wargames.storage.pairs.turns = 0
    wargames.storage.pairs.row = 0
  end
end

local function GetCardWindowForCardNumber( cardNumber )  
  local row = math.ceil(cardNumber / 8)
  local col = cardNumber - ((row - 1) * 8)
  return "WargamesPairsCardsRow"..row.."Col"..col
end

local function GetCardNumberForCardWindow( cardWindowName )  
  local col = WindowGetId( cardWindowName )
  local row = WindowGetId( WindowGetParent( cardWindowName ) )
  return col + (row - 1) * 8
end

local function GetCardNumberForCurrentWindow()  
  local cardWindowName = SystemData.ActiveWindow.name
  return GetCardNumberForCardWindow( cardWindowName ) 
end

local function PrepareGame()
  for i = 1, 64 do
    local bn = GetCardWindowForCardNumber(i)
	if wargames.storage.pairs.flipped[i - 1] then
      local texture, x, y = GetIconData(wargames.storage.pairs.cards[i - 1])
      DynamicImageSetTexture(GetCardWindowForCardNumber(i).."Icon", texture, x, y)
	else
	  DynamicImageSetTexture (GetCardWindowForCardNumber(i).."Icon", "", 0, 0)
	end

    WindowSetShowing(bn.."Text", false)
    WindowSetGameActionData(bn, 0, 0, L"")
    WindowSetShowing(bn.."Cooldown", false)
    WindowSetShowing(bn.."CooldownTimer", false)
  end
  d("Pairs prepared")
end

local function ResetGame()
  wargames.pairs.flip0 = -1
  wargames.pairs.flip1 = -1
  wargames.storage.pairs.cards = {}
  wargames.storage.pairs.flipped = {}
  wargames.storage.pairs.solved = 0
  wargames.storage.pairs.turns = 0
  wargames.storage.pairs.row = 0

  for i = 0, 63 do
    wargames.storage.pairs.cards[i] = wargames.storage.pairs.firsticonindex + (i % 32)
	wargames.storage.pairs.flipped[i] = false
  end
  
  -- simple shuffling function
  for i = 0, 1000 do
    local t = wargames.storage.pairs.cards[0]
	local r = math.random(64) - 1
	wargames.storage.pairs.cards[0] = wargames.storage.pairs.cards[r]
	wargames.storage.pairs.cards[r] = t
  end
  d("Pairs reset")
end

local function FlipCard(cardNum)
  PlaySound(120)
  wargames.storage.pairs.flipped[cardNum] = not wargames.storage.pairs.flipped[cardNum]
  if wargames.storage.pairs.flipped[cardNum] then
    local texture, x, y = GetIconData(wargames.storage.pairs.cards[cardNum])
    DynamicImageSetTexture(GetCardWindowForCardNumber(cardNum + 1).."Icon", texture, x, y)
  else
    DynamicImageSetTexture (GetCardWindowForCardNumber(cardNum + 1).."Icon", "", 0, 0)
  end
end

function wargames.pairs.OnInitialize()
  InitStorage()
  LabelSetText("WargamesPairsTitleBarText", L"Wargames Pairs")
  if wargames.storage.pairs.firsttime then
    wargames.storage.pairs.firsttime = false
	ResetGame()
  end
  PrepareGame()

  if LibAchievements then
    local wgcat = LibAchievements.CreateCategory(wargames.T.tomecategoryname)
    local pairs_page = LibAchievements.CreatePage(wgcat,wargames.T.pairs.tomepagename, true, wargames.T.pairs.tomepagedescription)
  
    --LibAchievements.AppendEntry(pairs_page,L"Statistiken",L"Gespielte Runden: 0\nBeste Runde: 0 gedrehte Karten\nBeste Leistung: 0 Paare ohne Fehler",function() return true end)
    LibAchievements.AppendEntry(pairs_page,wargames.T.pairs.an_completed,wargames.T.pairs.ad_completed,function() return wargames.storage.pairs.stats_completed >= 1 end)
    LibAchievements.AppendEntry(pairs_page,wargames.T.pairs.an_completed100,wargames.T.pairs.ad_completed100,function() return wargames.storage.pairs.stats_completed >= 100 end)
    LibAchievements.AppendEntry(pairs_page,wargames.T.pairs.an_row3,wargames.T.pairs.ad_row3,function() return wargames.storage.pairs.stats_row >= 3 end)
    LibAchievements.AppendEntry(pairs_page,wargames.T.pairs.an_row5,wargames.T.pairs.ad_row5,function() return wargames.storage.pairs.stats_row >= 5 end)
    LibAchievements.AppendEntry(pairs_page,wargames.T.pairs.an_turns100,wargames.T.pairs.ad_turns100,function() return wargames.storage.pairs.stats_turns <= 100 end)
    LibAchievements.AppendEntry(pairs_page,wargames.T.pairs.an_turns50,wargames.T.pairs.ad_turns50,function() return wargames.storage.pairs.stats_turns <= 50 end) 
  end
end

function wargames.pairs.OnShutdown()
  if wargames.pairs.flip0 ~= -1 then
    wargames.storage.pairs.flipped[wargames.pairs.flip0] = false
	wargames.pairs.flip0 = -1
    if wargames.pairs.flip1 ~= -1 then
      wargames.storage.pairs.flipped[wargames.pairs.flip1] = false
      wargames.pairs.flip1 = -1
	end
  end
end

function wargames.pairs.Hide()
  WindowSetShowing("WargamesPairs", false);
end

function wargames.pairs.CardLButtonDown()
  local i = GetCardNumberForCurrentWindow() - 1
  
  if wargames.storage.pairs.solved == 32 then
    wargames.storage.pairs.stats_completed = wargames.storage.pairs.stats_completed + 1
	if wargames.storage.pairs.stats_turns > wargames.storage.pairs.turns then
	  wargames.storage.pairs.stats_turns = wargames.storage.pairs.turns
	end
    ResetGame()
	PrepareGame()
	PlaySound(200)
  else
    if wargames.pairs.flip0 ~= -1 and wargames.pairs.flip1 ~= -1 then
      FlipCard(wargames.pairs.flip0)
      FlipCard(wargames.pairs.flip1)
      wargames.pairs.flip0 = -1
      wargames.pairs.flip1 = -1
    else
      if not wargames.storage.pairs.flipped[i] then
        if wargames.pairs.flip0 == -1 then
          FlipCard(i)
	      wargames.pairs.flip0 = i
          wargames.storage.pairs.turns = wargames.storage.pairs.turns + 1
        else
          if wargames.pairs.flip1 == -1 then
  	        FlipCard(i)
		    wargames.pairs.flip1 = i
            wargames.storage.pairs.turns = wargames.storage.pairs.turns + 1
		
	        if wargames.storage.pairs.cards[wargames.pairs.flip0] == wargames.storage.pairs.cards[wargames.pairs.flip1] then
  	          wargames.pairs.flip0 = -1
		      wargames.pairs.flip1 = -1
			  wargames.storage.pairs.solved = wargames.storage.pairs.solved + 1
			  wargames.storage.pairs.row = wargames.storage.pairs.row + 1
			  if wargames.storage.pairs.stats_row < wargames.storage.pairs.row then
			    wargames.storage.pairs.stats_row = wargames.storage.pairs.row
				d("Now "..wargames.storage.pairs.stats_row.." in a row")
			  end
			  PlaySound(219)
			else
			  wargames.storage.pairs.row = 0
	        end
		  end
	    end
      end
    end
  end
end

function wargames.pairs.CardLButtonUp()

end

function wargames.pairs.CardRButtonDown()

end

function wargames.pairs.CardMouseOver()

end

wargames.RegisterGame(L"Wargames Pairs", L"How about playing some pairs game?", "WargamesPairs", L"pairs", wargames.pairs.OnInitialize, wargames.pairs.OnShutdown, nil)
